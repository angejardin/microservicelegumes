package com.angejardin.microservicelegumes;

import javax.transaction.Transactional;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.angejardin.microservicelegumes.dao.LegumesRepository;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
class MicroservicelegumesApplicationTests {
	
	@LocalServerPort
    private int port;

    @Autowired
    private LegumesRepository repository;
    
    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();
    
    

	@Test
	void testGetListeLegumes() throws JSONException{
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/legumes/listeLegumes"),
                HttpMethod.GET, entity, String.class);

        String expected = "[\"ail\",\n" + 
        		"    \"asperge\",\n" + 
        		"    \"artichaud\",\n" + 
        		"    \"betterave\",\n" + 
        		"    \"carotte\",\n" + 
        		"    \"chou-fleur\",\n" + 
        		"    \"haricot\",\n" + 
        		"    \"laitue\",\n" + 
        		"    \"melon\",\n" + 
        		"    \"oignon couleur\",\n" + 
        		"    \"oignon blanc\",\n" + 
        		"    \"pomme de terre\",\n" + 
        		"    \"poireau\",\n" + 
        		"    \"tomate\",\n" + 
        		"    \"poivron\",\n" + 
        		"    \"piment\",\n" + 
        		"    \"aubergine\",\n" + 
        		"    \"courgette\"]";
        JSONAssert.assertEquals(expected, response.getBody(), false);		
	}
	
	
	
	@Test
	void testGetInfosLegume() throws JSONException{
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/legumes/infosLegume/ail"),
                HttpMethod.GET, entity, String.class);

        String expected = "{\n" + 
        		"    \"id\": 7,\n" + 
        		"    \"nom\": \"ail\",\n" + 
        		"    \"besoinEauCroissanceNord\": 0,\n" + 
        		"    \"besoinEauProductionNord\": 30,\n" + 
        		"    \"besoinEauCroissanceSud\": 30,\n" + 
        		"    \"besoinEauProductionSud\": 50,\n" + 
        		"    \"semisSousAbris\": [\n" + 
        		"        0\n" + 
        		"    ],\n" + 
        		"    \"semisEnPlace\": [\n" + 
        		"        0\n" + 
        		"    ],\n" + 
        		"    \"plantation\": [\n" + 
        		"        2,\n" + 
        		"        3,\n" + 
        		"        10,\n" + 
        		"        11\n" + 
        		"    ],\n" + 
        		"    \"recolte\": [\n" + 
        		"        7\n" + 
        		"    ]\n" + 
        		"}";
        JSONAssert.assertEquals(expected, response.getBody(), false);
		
		
	}
	
	
	private String createURLWithPort(String uri) {
        return "https://angejardin-sblegumes-staging.herokuapp.com" + uri;
    }

}
