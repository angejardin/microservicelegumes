/**
 * 
 */
package com.angejardin.microservicelegumes.controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.angejardin.microservicelegumes.dao.LegumesRepository;
import com.angejardin.microservicelegumes.models.Legumes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author Audrey Ledoux
 *
 */
@Api(description = "API pour les opérations CRUD pour les legumes") 
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController  
@RequestMapping(path="/legumes")
public class LegumesController {
	@Autowired  
    private LegumesRepository legumesRepository;
	
	@ApiOperation(value = "Donne toute la liste des légumes") 
	@GetMapping(path="/listeLegumes")  
    public @ResponseBody List<String> getAllLegumes() {		
        return legumesRepository.getLegumesNom();
    }
	
	@ApiOperation(value = "Donne les informations pour un légume") 
	@GetMapping(path="/infosLegume/{nom}")  
    public ResponseEntity<Legumes> getInfosLegume(@PathVariable String nom) {	
		Optional<Legumes> legumeData = legumesRepository.findByNom(nom);
		if (legumeData.isPresent()) {
			return new ResponseEntity<>(legumeData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }	
	
	@ApiOperation(value = "Enregistre un legume dans la base de données") 
	@PostMapping(path = "/addLegume")
	public ResponseEntity<Legumes> creerLegume(@Valid @RequestBody Legumes legume) {
		Legumes legumeAjoute = legumesRepository.save(legume);
		if (legumeAjoute == null)
			return ResponseEntity.noContent().build();
		
		return new ResponseEntity<>(legumeAjoute,HttpStatus.OK);
	}
	

}
