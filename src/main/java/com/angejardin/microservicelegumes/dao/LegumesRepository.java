/**
 * 
 */
package com.angejardin.microservicelegumes.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.angejardin.microservicelegumes.models.Legumes;

/**
 * @author Audrey Ledoux
 *
 */
public interface LegumesRepository extends CrudRepository<Legumes, Integer> {
	@Query("select l.nom from Legumes l")
    List<String> getLegumesNom();
	
	Optional<Legumes> findByNom(String nom);
	
}
