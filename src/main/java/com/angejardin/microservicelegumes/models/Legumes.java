/**
 * 
 */
package com.angejardin.microservicelegumes.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Audrey Ledoux
 *
 */
@Entity
public class Legumes {
	@Id    
    @GeneratedValue(strategy=GenerationType.AUTO)     
    private Integer id;

    private String nom;
    
    private Long besoinEauCroissanceNord;
    
    private Long besoinEauProductionNord;

    private Long besoinEauCroissanceSud;
    
    private Long besoinEauProductionSud;
    
    private Integer[] semisSousAbris;
    
    private Integer[] semisEnPlace;
    
    private Integer[] Plantation;
    
    private Integer[] Recolte;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the besoinEauCroissanceNord
	 */
	public Long getBesoinEauCroissanceNord() {
		return besoinEauCroissanceNord;
	}

	/**
	 * @param besoinEauCroissanceNord the besoinEauCroissanceNord to set
	 */
	public void setBesoinEauCroissanceNord(Long besoinEauCroissanceNord) {
		this.besoinEauCroissanceNord = besoinEauCroissanceNord;
	}

	/**
	 * @return the besoinEauProductionNord
	 */
	public Long getBesoinEauProductionNord() {
		return besoinEauProductionNord;
	}

	/**
	 * @param besoinEauProductionNord the besoinEauProductionNord to set
	 */
	public void setBesoinEauProductionNord(Long besoinEauProductionNord) {
		this.besoinEauProductionNord = besoinEauProductionNord;
	}

	/**
	 * @return the besoinEauCroissanceSud
	 */
	public Long getBesoinEauCroissanceSud() {
		return besoinEauCroissanceSud;
	}

	/**
	 * @param besoinEauCroissanceSud the besoinEauCroissanceSud to set
	 */
	public void setBesoinEauCroissanceSud(Long besoinEauCroissanceSud) {
		this.besoinEauCroissanceSud = besoinEauCroissanceSud;
	}

	/**
	 * @return the besoinEauProductionSud
	 */
	public Long getBesoinEauProductionSud() {
		return besoinEauProductionSud;
	}

	/**
	 * @param besoinEauProductionSud the besoinEauProductionSud to set
	 */
	public void setBesoinEauProductionSud(Long besoinEauProductionSud) {
		this.besoinEauProductionSud = besoinEauProductionSud;
	}

	/**
	 * @return the semisSousAbris
	 */
	public Integer[] getSemisSousAbris() {
		return semisSousAbris;
	}

	/**
	 * @param semisSousAbris the semisSousAbris to set
	 */
	public void setSemisSousAbris(Integer[] semisSousAbris) {
		this.semisSousAbris = semisSousAbris;
	}

	/**
	 * @return the semisEnPlace
	 */
	public Integer[] getSemisEnPlace() {
		return semisEnPlace;
	}

	/**
	 * @param semisEnPlace the semisEnPlace to set
	 */
	public void setSemisEnPlace(Integer[] semisEnPlace) {
		this.semisEnPlace = semisEnPlace;
	}

	/**
	 * @return the plantation
	 */
	public Integer[] getPlantation() {
		return Plantation;
	}

	/**
	 * @param plantation the plantation to set
	 */
	public void setPlantation(Integer[] plantation) {
		Plantation = plantation;
	}

	/**
	 * @return the recolte
	 */
	public Integer[] getRecolte() {
		return Recolte;
	}

	/**
	 * @param recolte the recolte to set
	 */
	public void setRecolte(Integer[] recolte) {
		Recolte = recolte;
	}

	/**
	 * @param id
	 * @param nom
	 * @param besoinEauCroissanceNord
	 * @param besoinEauProductionNord
	 * @param besoinEauCroissanceSud
	 * @param besoinEauProductionSud
	 * @param semisSousAbris
	 * @param semisEnPlace
	 * @param plantation
	 * @param recolte
	 */
	public Legumes(Integer id, String nom, Long besoinEauCroissanceNord, Long besoinEauProductionNord,
			Long besoinEauCroissanceSud, Long besoinEauProductionSud, Integer[] semisSousAbris, Integer[] semisEnPlace,
			Integer[] plantation, Integer[] recolte) {
		super();
		this.id = id;
		this.nom = nom;
		this.besoinEauCroissanceNord = besoinEauCroissanceNord;
		this.besoinEauProductionNord = besoinEauProductionNord;
		this.besoinEauCroissanceSud = besoinEauCroissanceSud;
		this.besoinEauProductionSud = besoinEauProductionSud;
		this.semisSousAbris = semisSousAbris;
		this.semisEnPlace = semisEnPlace;
		Plantation = plantation;
		Recolte = recolte;
	}

	/**
	 * 
	 */
	public Legumes() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	

}
